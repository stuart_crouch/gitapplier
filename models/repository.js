var mongoose = require('mongoose');


var RepositorySchema = new mongoose.Schema({
  name: { type: String, required: true},
  path: { type: String, required: true},
  username: { type: String, required: true},
  password: { type: String, required: true, select: false}
});

module.exports = mongoose.model('Repo', RepositorySchema);
