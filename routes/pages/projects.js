// Fake user database
var Project = require('../../models/project.js');

exports.list = function(req, res){

  Project.find({}, function(err, projects) {
    if (err) throw err;
    res.render('projects/projects', { title: 'Projects', projects: projects });
  });

};

exports.load = function(req, res, next){
  var id = req.params.id;
  req.user = users[id];
  if (req.user) {
    next();
  } else {
    var err = new Error('cannot find user ' + id);
    err.status = 404;
    next(err);
  }
};

exports.view = function(req, res){

  Project.findById(req.params.id, function(err, project) {
       if (err)
           throw err;
       res.render('projects/view', {
         title: 'Viewing project ' + req.project_id,
         project: project
       });
   });
};

exports.edit = function(req, res){
  res.render('project/edit', {
    title: 'Editing user ' + req.user.name,
    user: req.user
  });
};

exports.update = function(req, res){
  // Normally you would handle all kinds of
  // validation and save back to the db
  var user = req.body.user;
  req.user.name = user.name;
  req.user.email = user.email;
  res.redirect('back');
};
