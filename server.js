var express = require('express');
var bodyParser = require('body-parser')
var errorhandler = require('errorhandler');
var mongoose = require('mongoose');

var config = require('./config');
var dashboard = require('./routes/pages/dashboard')
var projects = require('./routes/pages/projects');
var projectsAPI = require('./routes/api/v1/projects');
var repoAPI = require('./routes/api/v1/repositories')
var folders = require('./routes/pages/folders');
//var projectMODEL = require('./models/project.js');

// Path to our public directory
var pub = __dirname;

// setup middleware
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json())

app.use(express.static(pub));
app.use(errorhandler());
module.exports = app;

mongoose.connect(config.database);

// Set our default template engine to "jade"
// which prevents the need for extensions
// (although you can still mix and match)
app.set('view engine', 'jade');
// Pretty print the output
if (app.get('env') === 'development') {
  app.locals.pretty = true;
}

var routerTest = express.Router();

routerTest.get('/', function(req, res) {
  res.json({message: "test"});
});

app.get('/', dashboard.dashboard);
app.get('/projects', projects.list);
app.get('/project/:id', projects.view);

app.use('/api/v1/projects/', projectsAPI);
app.use('/api/v1/repositories/', repoAPI);
app.use('/api', routerTest);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.listen(3000,function(){
  console.log("Live at Port 3000");
});
