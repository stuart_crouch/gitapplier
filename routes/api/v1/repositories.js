var Repo = require('../../../models/repository');
var express = require('express');
var router = express.Router();


router.use(function(req, res, next) {
  console.log("repository call");
  next();
});

router.route('/repository')
  .post(function(req, res) {
    var data = req.body;
    console.log(data);
    if (!data.name || !data.path) {
        res.status(400);
        res.json({
            "error": "Invalid Data"
        });
    } else {

      var dbObj = new Repo();
      dbObj.name = data.name;
      dbObj.path = data.path;
      dbObj.username = data.username;
      dbObj.password = data.password;

      dbObj.save(dbObj, function(err, result) {
          if (err)
              res.send(err);

          res.json(result);
      })
    }
  });

router.route('/repos')
  .get(function(req, res) {
    Repo.find(function(err, result) {
      if (err)
        res.send(err);

      res.json(result);
    });
  });

router.route('/repos/:id')
  .get(function(req, res) {
    Repo.findById(req.params.id, function(err, result) {
      if (err)
        res.send(err);

      res.json(result);
    });
  })
  .put(function(req, res) {
    Repo.findById(req.params.id, function(err, dbObj) {
      if (err)
        res.send(err);

      dbObj.name = req.body.name;

      dbObj.save(function(err, result) {
        if (err)
          res.send(err);

        res.json(result);
      });
    });
  })
  .delete(function(req, res) {
    Repo.remove({
      _id: req.params.id
    }, function(err, dbObj) {
      if (err)
        res.send(err);

      res.json( { message: req.params.id + " deleted" } );
    });
  });


module.exports = router;
