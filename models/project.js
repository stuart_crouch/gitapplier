var mongoose = require('mongoose');

var ProjectSchema = new mongoose.Schema({
  name: String,
  repositories: [{type: mongoose.Schema.Types.ObjectId, ref: 'Repositories'}]
});

module.exports = mongoose.model('Project', ProjectSchema);
