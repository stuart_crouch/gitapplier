$(document).ready(function() {
  $("#add").submit(function(event){
      // cancels the form submission
      event.preventDefault();

      var $form = $(this);
      var $target = $($form.attr('data-target'));

      $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),

          data: $form.serialize(),
          success: function(result) {
            $("#addWidgetModal").animate({scrollTop:0}, 'slow');
            $("#addWidgetModal").hide();
            window.location.reload(true);
          },
          error: function (request, status, error) {
              alert(request.responseText);
          }
      });
  });
});
