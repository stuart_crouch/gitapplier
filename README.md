# GIT applier (gitapplier) #

This project is intended to be a node.js server that provides a web interface allowing users to clone and update git repositories.  It does nothing more, and is not intended as a web based git client.

The purpose behind it is to provide a simple way to allow users to commit their changes in git and then pull them when ready, but without providing commandline access to the host (hence the web interface)

## Installation ##

Pull the repository
use a command line to run


```
npm install
```


## Running ##

Start via the command line using


```
mongo.bat &
npm start
```

This will start nodemon watching for changes, so the server will restart after any js or json change
Visit the site at port 3000 (Eg http://localhost:3000)

