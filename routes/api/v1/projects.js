var Project = require('../../../models/project.js');
var express = require('express');
var router = express.Router();

router.use(function(req, res, next) {
  console.log("something happened");
  next();
});

router.get('/', function(reqs, res, next) {
    Project.find(function(err, data) {
      if (err)
        res.send(err);

      res.json(data);
    });
});

router.route('/project')
  .post(function(req, res) {
    var data = req.body;
    if (!data.name) {
        res.status(400);
        res.json({
            "error": "Invalid Data"
        });
    } else {

      var project = new Project();
      project.name = data.name;

      project.save(project, function(err, result) {
          if (err)
              res.send(err);

          res.json(result);
      })
    }
  });

router.route('/projects')
  .get(function(req, res) {
    Project.find(function(err, projects) {
      if (err)
        res.send(err);

      res.json(projects);
    });
  });

router.route('/projects/:project_id')
  .get(function(req, res) {
    Project.findById(req.params.project_id, function(err, project) {
      if (err)
        res.send(err);

      res.json(project);
    });
  })
  .put(function(req, res) {
    Project.findById(req.params.project_id, function(err, project) {
      if (err)
        res.send(err);

      project.name = req.body.name;

      project.save(function(err, result) {
        if (err)
          res.send(err);

        res.json(result);
      });
    });
  })
  .delete(function(req, res) {
    Project.remove({
      _id: req.params.project_id
    }, function(err, project) {
      if (err)
        res.send(err);

      res.json( { message: req.params.project_id + " deleted" } );
    });
  });


module.exports = router;
